# functions
function New-TemporaryDirectory
{
    $parent = [System.IO.Path]::GetTempPath()
    [string] $name = [System.Guid]::NewGuid()
    New-Item -ItemType Directory -Path (Join-Path $parent $name)
}

function Install-Config
{
    param(
        [string]$name,
        [System.IO.FileInfo]$destination,
        [switch]$Online
    )
    if ($Online)
    {
            Invoke-WebRequest -Uri "$CONFIG_BASE_URL/$name" -Credential $CREDENTIALS -OutFile "$TEMP_FOLDER\$name.zip"
            Expand-Archive -Path "$TEMP_FOLDER\$name.zip" -DestinationPath $destination
    }
    else
    {
        Move-Item -Path ".\config\$Name" -Destination $Destination
    }

}

# base config variables
$TEMP_FOLDER = New-TemporaryDirectory
$CONFIG_BASE_URL = 'https://deepspace2.cmdcentral.xyz/protected/setup-scripts/windows/config'
$CREDENTIALS = Get-Credential -Message "Enter credentials for protected config download."

# install boxstarter
. { Invoke-WebRequest -useb https://boxstarter.org/bootstrapper.ps1 } | Invoke-Expression
Get-Boxstarter -Force

# basic boxstarter config
Disable-InternetExplorerESC
Disable-GameBarTips
Disable-BingSearch
Enable-RemoteDesktop
Enable-MicrosoftUpdate
Set-WindowsExplorerOptions -EnableShowHiddenFilesFoldersDrives -EnableShowProtectedOSFiles -EnableShowFileExtensions
Set-TaskbarOptions -Size Small -Lock -Dock Right -Combine Full

# chocolatey config/options
choco feature enable -n allowGlobalConfirmation

# chocolatey packages
$chocoPackages = @(
    "7zip.install"
    "keypirinha"
    "firefox"
    "vscode"
    "openvpn"
    "vlc"
    "FoxitReader"
    "Bitwarden"
    "notepadplusplus"
    "autohotkey.install"
    "git --params /GitOnlyOnPath /NoShellIntegration /Schannel"
    "poshgit"
    "GPMDP" # Google Play Music Desktop Player
    "logitech-options"
    "Microsoft-Windows-Subsystem-Linux -Source WindowsFeatures"
    "sharpkeys"
)

$chocoOptionalPackages = @(
    "docker-for-windows"
    "microsoft-teams"
    "Microsoft-Hyper-V-All -Source WindowsFeatures"
    "dotnetcore"
    "powershell-core"
    "powertoys"
    "alacritty"
)

$storeApps = @(
    "9N0DX20HK701" # Windows Terminal
    "9NZTWSQNTD0S" # Telegram Desktop
    "9NBLGGH4MSV6" # Ubuntu
)

foreach ($package in $chocoPackages)
{
    Write-Host "Installing $package..."
    Invoke-Expression "cinst $package"
    Write-Host "Successfully installed $package"
}

foreach ($package in $chocoOptionalPackages)
{
    $choice = Read-Host -Prompt "Would you like to install ${package}? [y/N]"
    if ($choice.ToLower() -like 'y*')
    {
        Write-Host "Installing $package..."
        Invoke-Expression "cinst $package"
        Write-Host "Successfully installed $package"
    }
}

foreach ($package in $storeApps)
{
    Write-Host "Prompting to install store app ${package}..."
    Start-Process "ms-windows-store://pdp/?ProductId=$package"
    Read-Host -Prompt "Press Enter to continue"
}

$undesiredPackages = @(
    "Microsoft.3DBuilder"
    "Microsoft.GetStarted"
    "king.com.CandyCrush*"
    "Microsoft.Bing*"
    "Microsoft.Xbox*"
    "Microsoft.Zune*"
)

foreach ($package in $undesiredPackages)
{
    Get-AppxPackage $package -ErrorAction SilentlyContinue | Remove-AppxPackage
}

# posh modules/config
Install-PackageProvider -Name NuGet -MinimumVersion 2.8.5.201 -Force
Install-Module DirColors -Force
Install-Module posh-git -Force

# misc settings - telemetry and experience
# Privacy: Let apps use my advertising ID: Disable
If (-Not (Test-Path "HKCU:\SOFTWARE\Microsoft\Windows\CurrentVersion\AdvertisingInfo"))
{
    New-Item -Path HKCU:\SOFTWARE\Microsoft\Windows\CurrentVersion\AdvertisingInfo | Out-Null
}
Set-ItemProperty -Path HKCU:\SOFTWARE\Microsoft\Windows\CurrentVersion\AdvertisingInfo -Name Enabled -Type DWord -Value 0

# WiFi Sense: HotSpot Sharing: Disable
If (-Not (Test-Path "HKLM:\Software\Microsoft\PolicyManager\default\WiFi\AllowWiFiHotSpotReporting"))
{
    New-Item -Path HKLM:\Software\Microsoft\PolicyManager\default\WiFi\AllowWiFiHotSpotReporting | Out-Null
}
Set-ItemProperty -Path HKLM:\Software\Microsoft\PolicyManager\default\WiFi\AllowWiFiHotSpotReporting -Name value -Type DWord -Value 0

# WiFi Sense: Shared HotSpot Auto-Connect: Disable
Set-ItemProperty -Path HKLM:\Software\Microsoft\PolicyManager\default\WiFi\AllowAutoConnectToWiFiSenseHotspots -Name value -Type DWord -Value 0

# Start Menu: Disable Bing Search Results
Set-ItemProperty -Path HKCU:\SOFTWARE\Microsoft\Windows\CurrentVersion\Search -Name BingSearchEnabled -Type DWord -Value 0
# To Restore (Enabled):
# Set-ItemProperty -Path HKCU:\SOFTWARE\Microsoft\Windows\CurrentVersion\Search -Name BingSearchEnabled -Type DWord -Value 1

# Change Explorer home screen back to "This PC"
Set-ItemProperty -Path HKCU:\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\Advanced -Name LaunchTo -Type DWord -Value 1
# Change it back to "Quick Access" (Windows 10 default)
# Set-ItemProperty -Path HKCU:\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\Advanced -Name LaunchTo -Type DWord -Value 2

# Better File Explorer
Set-ItemProperty -Path HKCU:\Software\Microsoft\Windows\CurrentVersion\Explorer\Advanced -Name NavPaneExpandToCurrentFolder -Value 1		
Set-ItemProperty -Path HKCU:\Software\Microsoft\Windows\CurrentVersion\Explorer\Advanced -Name NavPaneShowAllFolders -Value 1		
Set-ItemProperty -Path HKCU:\Software\Microsoft\Windows\CurrentVersion\Explorer\Advanced -Name MMTaskbarMode -Value 2

# Disable the Lock Screen (the one before password prompt - to prevent dropping the first character)
If (-Not (Test-Path HKLM:\SOFTWARE\Policies\Microsoft\Windows\Personalization))
{
    New-Item -Path HKLM:\SOFTWARE\Policies\Microsoft\Windows -Name Personalization | Out-Null
}
Set-ItemProperty -Path HKLM:\SOFTWARE\Policies\Microsoft\Windows\Personalization -Name NoLockScreen -Type DWord -Value 1
# To Restore:
# Set-ItemProperty -Path HKLM:\SOFTWARE\Policies\Microsoft\Windows\Personalization -Name NoLockScreen -Type DWord -Value 1

# Disable Xbox Gamebar
Set-ItemProperty -Path "HKCU:\SOFTWARE\Microsoft\Windows\CurrentVersion\GameDVR" -Name AppCaptureEnabled -Type DWord -Value 0
Set-ItemProperty -Path "HKCU:\System\GameConfigStore" -Name GameDVR_Enabled -Type DWord -Value 0

# Turn off People in Taskbar
If (-Not (Test-Path "HKCU:SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\Advanced\People"))
{
    New-Item -Path HKCU:SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\Advanced\People | Out-Null
}
Set-ItemProperty -Path "HKCU:SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\Advanced\People" -Name PeopleBand -Type DWord -Value 0

# enable clipboard history
# yes, this is counterintuitive.
Remove-ItemProperty -Path 'HKLM:\SOFTWARE\Policies\Microsoft\Windows\System' -Name AllowClipboardHistory

# config various things
Install-Config -name 'keypirinha' -destination "$env:APPDATA\Keypirinha"
Install-Config -name 'autohotkey' -destination "$env:USERPROFILE\Documents"
Install-Config -name 'vscode'     -destination "$env:APPDATA\Code\User"

# config VisualStudioCode
refreshenv # choco thing to reload path &c
code --install-extension Shan.code-settings-sync

# add dvorak layout
$currentLayout = Get-WinUserLanguageList
$currentLayout[0].InputMethodTips.Add("0409:00010409")
Set-WinUserLanguageList -LanguageList $currentLayout

# finally, get all remaining updates
Install-WindowsUpdate