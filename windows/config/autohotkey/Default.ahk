^!r::Reload ; ctrl+alt+r will reload this script

::.thx::
(


Thanks,
Braxton
)

::.questions::
(

Let me know if you have any questions.

Thanks,
Braxton
)

; diacritics, latex style
:*?:\'a::á
:*?:\'e::é
:*?:\'i::í
:*?:\'o::ó
:*?:\'u::ú
:*?:\~n::ñ

::.today::
FormatTime, TimeString, , yyyy-MM-dd
Send, %TimeString%
return

::.now::
FormatTime, TimeString, , yyyy-MM-dd HH:mm
Send, %TimeString%
return

::.nows::
FormatTime, TimeString, , yyyy-MM-dd HH:mm
Send, %TimeString% .bjs
return

::.scriptheader::
(
###############################################################################
#
# <description>
#
#     Usage: ./<scriptname>
#
#  Homepage: https://catlab.epic.com/<projectpath>
#
#    Author: Braxton Schafer <braxton@epic.com> (bjs)
#
# Changelog:
#   - <today> .bjs:  Initial creation
#
###############################################################################
)