#!/bin/bash
##############################################################################
#
# Sets a static IP on a linux box
# Supports:
#  - Ubuntu <= 16.04
#  - CentOS/RHEL 7
#
#     Usage: ./set-static-ip.sh --ip-address 10.0.0.5 --netmask [24 | 255.255.255.0] --gateway 10.0.0.1 --dns-servers 10.0.0.1,8.8.8.8,8.8.4.4 --dns-domain cmdcentral.xyz
#
#    Author: Braxton Schafer <braxton@cmdcentral.xyz> (bjs)
#
###############################################################################
set -e

function sexit() {
	echo "$1" >>/var/log/setup-scripts/set-static-ip
	[ $VERBOSE ] && echo "EXITING: $1"
	exit 1
}

function slog() {
	echo "$1" >>/var/log/setup-scripts/set-static-ip
	[ $VERBOSE ] && echo "$1"
}

function print_help() {
	echo "Read the code, dummy"
	exit 0
}

# separates them commas
function fix_dns_servers() {
	DNS_SERVERS=$(echo $DNS_RAW | awk -F, '{ for (i=1 ; i <= NF ; i++) print "nameserver " $i}')
}

function get_distro_name() {
	if [ -f '/etc/centos-release' ]; then
		DISTRO_NAME='CentOS'
		DISTRO_VERSION=$(cat /etc/centos-release | awk '{ print $4 }')

		if [ "$DISTRO_VERSION" -ne '7' ]; then
			sexit "Detected distro as $DISTRO_NAME $DISTRO_VERSION. This is not currently supported."
		fi

	elif [ -f '/etc/rhel-release' ]; then
		DISTRO_NAME='RHEL'
		DISTRO_VERSION=$(cat /etc/rhel-release | awk '{ print $4 }')

		if [ "$DISTRO_VERSION" -ne '7' ]; then
			sexit "Detected distro as $DISTRO_NAME $DISTRO_VERSION. This is not currently supported."
		fi

	elif [ -f '/etc/lsb-release' ]; then
		DISTRO_NAME='Ubuntu'
		DISTRO_VERSION=$(cat /etc/lsb-release | grep RELEASE | awk -F= '{ print $2 }')

		if [ "$(echo $DISTRO_VERSION | awk -F. '{ print $1 }')" -gt '16' ]; then
			sexit "Detected distro as $DISTRO_NAME $DISTRO_VERSION. This is not currently supported."
		fi
	fi

}

function get_adapter_name() {
	ADAPTER_NAME="$(ip addr show | grep -v lo | egrep '[0-9]+: .*' | awk '{ print $2 }' | awk -F: '{ print $1 }')"
}

function set_ip_centos() {
	cp "/etc/sysconfig/network-scripts/ifcfg-$ADAPTER_NAME" ~

	cat <<EOF >"/etc/sysconfig/network-scripts/ifcfg-$ADAPTER_NAME"
NM_CONTROLLED="no"
ONBOOT="yes"
BOOTPROTO="static"
IPADDR=$IP_ADDRESS
NETMASK=$NETMASK
GATEWAY=GATEWAY
EOF

}

function set_dns() {
	echo $DNS_SERVERS >/etc/resolv.conf
	echo "search $DNS_DOMAIN" >>/etc/resolv.conf
}

OPTS=$(getopt -o i:n:g:s:d:vh --long ip-address:,netmask:,gateway:,dns-servers:,dns-domain:,verbose,help, -n 'parse-options' -- "$@")

if [ $? != 0 ]; then
	echo "Failed parsing options." >&2
	exit 1
fi

echo "$OPTS"
eval set -- "$OPTS"

VERBOSE=0

while true; do
	case "$1" in
	-i | --ip-address)
		IP_ADDRESS="$2"
		shift
		shift
		;;
	-n | --netmask)
		NETMASK="$2"
		shift
		shift
		;;
	-g | --gateway)
		GATEWAY="$2"
		shift
		shift
		;;
	-s | --dns-servers)
		DNS_RAW="$2"
		shift
		shift
		;;
	-d | --dns-domain)
		DNS_DOMAIN="$2"
		shift
		shift
		;;
	-v | --verbose)
		VERBOSE=1
		shift
		;;
	-h | --help)
		print_help
		shift
		;;
	-s | --stack-size)
		STACK_SIZE="$2"
		shift
		shift
		;;
	--)
		shift
		break
		;;
	*) break ;;
	esac
done

if [ $UID -ne 0 ]; then
	echo "This must be run as root. Exiting."
	exit 2
fi
